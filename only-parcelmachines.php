<?php
/*
Plugin Name: Only Parcelmachines
Description: Removes billing address fields except for phone field
Author: Yotech OÜ
Author URI: http://yotech.ee
Version: 1.0.0
*/

add_filter('woocommerce_billing_fields', 'wpb_custom_billing_fields');
function wpb_custom_billing_fields( $fields = array() ) {

    unset($fields['billing_company']);
    unset($fields['billing_address_1']);
    unset($fields['billing_address_2']);
    unset($fields['billing_state']);
    unset($fields['billing_city']);
    unset($fields['billing_postcode']);

    return $fields;
}